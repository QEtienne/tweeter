<?php
    session_start();
    require_once 'vendor/autoload.php';
    require_once 'src/mf/utils/ClassLoader.php';
    new mf\utils\ClassLoader('src');
    use tweeterapp\control\TweeterController as Tweeter;
    use mf\router\Router as Router;
    use tweeterapp\auth\TweeterAuthentification as Auth;

    mf\view\AbstractView::addStyleSheet('html/css/main.css');   

    $config = parse_ini_file('conf/conf.ini');
    $db = new Illuminate\Database\Capsule\Manager();
    $db->addConnection($config); 
    $db->setAsGlobal();
    $db->bootEloquent();

    $router = new Router();
    $router->addRoute('maison'     , '/home/'                , '\tweeterapp\control\TweeterController'     , 'viewHome'          , Auth::ACCESS_LEVEL_NONE);
    $router->addRoute('tweet'      , '/view/'                , '\tweeterapp\control\TweeterController'     , 'viewTweet'         , Auth::ACCESS_LEVEL_NONE);
    $router->addRoute('utilisateur', '/user/'                , '\tweeterapp\control\TweeterController'     , 'viewUserTweets'    , Auth::ACCESS_LEVEL_NONE);
    $router->addRoute('mapage'     , '/personalHome/'        , '\tweeterapp\control\TweeterController'     , 'viewPersonalHome'  , Auth::ACCESS_LEVEL_USER);
    $router->addRoute('formTweet'  , '/post/'                , '\tweeterapp\control\TweeterController'     , 'viewPostTweet'     , Auth::ACCESS_LEVEL_USER);
    $router->addRoute('newTweet'   , '/send/'                , '\tweeterapp\control\TweeterController'     , 'sendTweet'         , Auth::ACCESS_LEVEL_USER);
    $router->addRoute('like'       , '/like/'                , '\tweeterapp\control\TweeterController'     , 'likeTweet'         , Auth::ACCESS_LEVEL_USER);
    $router->addRoute('dislike'    , '/dislike/'             , '\tweeterapp\control\TweeterController'     , 'dislikeTweet'      , Auth::ACCESS_LEVEL_USER);
    $router->addRoute('follow'     , '/follow/'              , '\tweeterapp\control\TweeterController'     , 'followSomeone'     , Auth::ACCESS_LEVEL_USER);
    $router->addRoute('following'  , '/following/'           , '\tweeterapp\control\TweeterController'     , 'viewFollowed'      , Auth::ACCESS_LEVEL_USER);
    $router->addRoute('loginForm'  , '/login/'               , '\tweeterapp\control\TweeterAdminController', 'viewLogin'         , Auth::ACCESS_LEVEL_NONE);
    $router->addRoute('connexion'  , '/checkLogin/'          , '\tweeterapp\control\TweeterAdminController', 'checkLogin'        , Auth::ACCESS_LEVEL_NONE);
    $router->addRoute('deconnexion', '/logout/'              , '\tweeterapp\control\TweeterAdminController', 'logout'            , Auth::ACCESS_LEVEL_USER);
    $router->addRoute('signupform' , '/signup/'              , '\tweeterapp\control\TweeterAdminController', 'viewSignup'        , Auth::ACCESS_LEVEL_NONE);
    $router->addRoute('signup'     , '/check_signup/'        , '\tweeterapp\control\TweeterAdminController', 'checkSignUp'       , Auth::ACCESS_LEVEL_NONE);
    $router->addRoute('admin'      , '/admin/'               , '\tweeterapp\control\TweeterAdminController', 'viewAdmin'         , Auth::ACCESS_LEVEL_ADMIN);
    $router->addroute('listUser'  , '/admin/list/'          , '\tweeterapp\control\TweeterAdminController', 'viewUsersList'     , Auth::ACCESS_LEVEL_ADMIN);
    $router->addRoute('followers'  , '/admin/list/follwers/' , '\tweeterapp\control\TweeterAdminController', 'viewUserFollowers' , Auth::ACCESS_LEVEL_ADMIN);
    $router->addRoute('sphere'     , '/admin/sphere/'        , '\tweeterapp\control\TweeterAdminController', 'viewSphere'        , Auth::ACCESS_LEVEL_ADMIN);
    $router->setDefaultRoute('/home/');

    $router->run();
?>