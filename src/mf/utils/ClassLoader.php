<?php
  namespace mf\utils;

  class ClassLoader {
    private $prefix;
    private $autoload;

    public function __construct($classfolder) {
      $this->prefix = $classfolder . "/";
      /*
      * On peux charger les auloloads directement à l'instanciation
      */
      spl_autoload_register(array($this, 'loadClass'));
    }

    private function loadClass(String $classname) {
      $replace = array("\\" => "/");
      $pathclass = strtr($classname, $replace);
      $pathclass = $this->prefix . $pathclass;
      $pathclass .= ".php";
      // var_dump($pathclass); /* pour débug le chargement des classes */
      if (file_exists($pathclass)) {
        require_once($pathclass);
      }
    }

    /*
    * Ou alors, on peux utiliser une méthode pour enregistrer les autoloads
    */

    // public function register() {
    //   spl_autoload_register(array($this, 'loadClass'));
    //               /*
    //               * Ou alors en utilisant une fonction anonyme :
    //               */
    //   // spl_autoload_register(function ($class) {
    //   //   $replace = array("\\" => "/");
    //   //   $pathclass = strtr($class, $replace);
    //   //   $pathclass = $this->prefix . $pathclass;
    //   //   $pathclass .= ".php";
    //   //
    //   //   if (file_exists($pathclass)) {
    //   //     require_once($pathclass);
    //   //   }
    //   // });
    //
    // }
  }
 ?>
