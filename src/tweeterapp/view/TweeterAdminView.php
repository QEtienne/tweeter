<?php
    namespace  tweeterapp\view;

    use mf\router\Router;
    use tweeterapp\auth\TweeterAuthentification as Auth;

    class TweeterAdminView extends \mf\view\AbstractView {
        
        public function __construct($data) {
            parent::__construct($data);
        }

        private function renderHeader() {
            $router = new Router();
            $html = "<header class='theme-backcolor1'>
                        <h1>Admin</h1>
                        <nav id='nav-menu'>
                            <a href='" . $router->urlFor('maison') . "' class='button'>
                                <img src='" . $router->iconesPath() . "home.png' width='32' height='32' alt='Accueil'/>
                            </a>
                            <a href='" . $router->urlFor('listUser') . "'  class='button'>
                                Liste utilisteurs par nombre de followers
                            </a>
                            <a href='" . $router->urlFor('sphere') . "'  class='button'>
                                Liste utilisateurs par taille de sphère d'influence
                            </a>
                        </nav>
                    </header>";
            return $html;
        }

        private function renderFooter() {
            return "\t\t<footer class='theme-backcolor1'>Admin créée en Licence Pro &copy;2019</footer>";
        }

        private function renderList() {
            $router = new Router();
            $html = "<div>";
            foreach ($this->data as $user) {
                $html .= "<p><a href='" . $router->urlFor('followers', ['id' => $user->id]) . "'>" . $user->fullname . "</a> : " . $user->followers . " followers</p>";
            }
            $html .= "</div>";
            return $html;
        }

        private function renderFollowers() {
            $router = new Router();
            $html = "<div>";
            foreach ($this->data as $follower) {
                $html .= "<p><a href='" . $router->urlFor('utilisateur', ['id' => $follower->id]) . "'>" . $follower->fullname . "</a></p>"; 
            }
            $html .= "</div>";
            return $html;
        }

        private function renderSphere() {
            $html = "<div>";
            foreach ($this->data as $user) {
                $html .= "<p>" . $user[0]->fullname . " taille de la sphère d'influence : " . $user[1] . "</p>";
            
            }
            $html .= "</div>";
            return $html;
        }

        protected function renderBody($selector) {
            $html = $this->renderHeader();
            $html .= "<section class='theme-backcolor2'>";
            switch ($selector) {
                case 'list':
                    $html .= $this->renderList();
                    break;
                case 'followers':
                    $html .= $this->renderFollowers();
                    break;
                case 'sphere':
                    $html .= $this->renderSphere();
                    break;
            }
            $html .= "</section>";
            $html .= $this->renderFooter();
            return $html;
        }
    }
?>