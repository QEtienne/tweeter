<?php

namespace tweeterapp\view;

use mf\router\Router;
use tweeterapp\auth\TweeterAuthentification as Auth;
use tweeterapp\model\User;

class TweeterView extends \mf\view\AbstractView {
  
    /* Constructeur 
    *
    * Appelle le constructeur de la classe parent
    */
    public function __construct( $data ){
        parent::__construct($data);
    }

    /* Méthode renderHeader
     *
     *  Retourne le fragment HTML de l'entête (unique pour toutes les vues)
     */ 
    private function renderHeader(){
        $router = new Router();
        $auth = new Auth();
        if ($auth->logged_in) {
            $level = User::select('level')
                            ->where('username', '=', $_SESSION['user_login'])
                            ->first();
            $menu = "\t<nav id='nav-menu'>
                        <a href='" . $router->urlfor("maison") . "' class='button'>
                            <img src='" . $router->iconesPath() . "home.png' alt='Accueil' width='32' height='32' />
                        </a>
                        <a href='" . $router->urlFor("mapage") . "' class='button'>
                            <img src='' alt='Ma page perso' width='32' height='32' />
                        </a>
                        <a href='" . $router->urlfor("following") . "' class='button'>
                            <img src='" . $router->iconesPath() . "user.png' width='32' height='32' />
                        </a>
                        <a href='" . $router->urlFor('deconnexion') . "' class='button'>
                            <img src='" . $router->iconesPath() . "exit.png' width='32' height='32' />
                        </a>";
            if ($level->level == $auth::ACCESS_LEVEL_ADMIN) {
                $menu .= "<a href='" . $router->urlFor('admin') . "'>
                            <img src='' width='32' height='32' alt='Admin' />
                        </a>";
            }
            $menu .= "</nav>";
        } else {
            $menu = "\t<nav id='nav-menu'>
                        <a href='" . $router->urlfor("maison") . "' class='button'>
                            <img src='" . $router->iconesPath() . "home.png' width='32' height='32' />
                        </a>
                        <a href='" . $router->urlfor("loginForm") . "' class='button'>
                            <img src='" . $router->iconesPath() . "login.png' width='32' height='32' />
                        </a>
                        <a href='" . $router->urlFor('signupform') . "' class='button'>
                            <img src='" . $router->iconesPath() . "add.png' width='32' height='32' />
                        </a>
                    </nav>";
        }

        $html = "<header class='theme-backcolor1'>\n\t<h1>MiniTweetR</h1>\n" . $menu . "</header>\n";
        return $html;
    }
    
    /* Méthode renderFooter
     *
     * Retourne le fragment HTML du bas de la page (unique pour toutes les vues)
     */
    private function renderFooter(){
        return "\t\t<footer class='theme-backcolor1'>La super app créée en Licence Pro &copy;2019</footer>";

    }

    /* Méthode renderHome
     *
     * Vue de la fonctionalité afficher tous les Tweets. 
     *  
     */
    
    private function renderHome(){

        /*
         * Retourne le fragment HTML qui affiche tous les Tweets. 
         *  
         * L'attribut $this->data contient un tableau d'objets tweet.
         * 
         */
        $auth = new Auth();
        $html = "\t\t\t <h3>Latest Tweets</h3>";
        $router = new Router();
        foreach ($this->data as $tweet) {
            $date = explode(" ", $tweet->created_at);
            $author = $tweet->author()->first();
            $html .= "\t\t\t<div class='tweet'>\n";
            $html .=    "\t\t\t\t<a href='" . $router->urlFor("tweet", ['id' => $tweet->id]) . " class='tweet-text'>";
            $html .=        $tweet->text;
            $html .=    "\t\t\t\t</a><br>\n";
            $html .=    "\t\t\t\t<span class='tweet-footer'>" . $date[1] . " | " . $date[0] . "<a href='" . $router->urlFor("utilisateur", ['id' => $author->id]) . "' class='tweet-author'/>" . $author->username . "</a><span>\n";
            $html .= "\t\t\t</div>\n";
        }
        return $html;
        
    }
  
    /* Méthode renderUeserTweets
     *
     * Vue de la fonctionalité afficher tout les Tweets d'un utilisateur donné. 
     * 
     */
     
    private function renderUserTweets(){

        /* 
         * Retourne le fragment HTML pour afficher
         * tous les Tweets d'un utilisateur donné. 
         *  "
         * L'attribut $this->data contient un objet User.
         *
         */
        $router = new Router();
        $auth = new Auth();
        $tweets = $this->data->tweets()
                            ->orderBy('created_at', 'desc')
                            ->get();
        $html = "\t\t\t<div>\n";
        $html .= "\t\t\t\t<h2>tweets from " . $this->data->fullname . "</h2>\n";
        $html .= "\t\t\t\t<h3>" . $this->data->followers . " followers</h3>\n";
        $html .= "\t\t\t</div>\n";
        foreach ($tweets as $tweet) {
            $date = explode(" ", $tweet->created_at);
            $html .= "\t\t\t\t<div class='tweet'>\n";
            $html .=    "\t\t\t\t\t<a class='tweet-text' href='" . $router->urlFor("tweet", ['id' => $tweet->id]) . "'>";
            $html .=        $tweet->text;
            $html .=    "</a><br>\n";
            $html .=   "\t\t\t\t\t" . $date[1] . " | " . $date[0] . "<a class='tweet-author' href='" . $router->urlFor("utilisateur", ['id' => $this->data->id]) . "'>" . $this->data->username . "</a>\n";
            $html .= "\t\t\t\t</div>\n";
        }
        return $html;
    }
  
    /* Méthode renderViewTweet 
     * 
     * Rréalise la vue de la fonctionnalité affichage d'un tweet
     *
     */
    
    private function renderViewTweet(){

        /* 
         * Retourne le fragment HTML qui réalise l'affichage d'un tweet 
         * en particulié 
         * 
         * L'attribut $this->data contient un objet Tweet
         *
         */
        $auth = new Auth();
        $router = new Router();
        $date = explode(" ", $this->data->created_at);
        $author = $this->data->author()
                            ->first();
        $html = "\t\t\t<div class='tweet'>\n";
        $html .=    "\t\t\t\t<a href='" . $router->urlFor("tweet", ['id' => $this->data->id]) . "'>";
        $html .=        $this->data->text;
        $html .=    "</a><br>\n";
        $html .=    "\t\t\t\t" . $date[1] . " | " . $date[0] . " - " . "<a href='" . $router->urlFor("utilisateur", ['id' => $author->id]) . "'>" . $author->username . "</a>\n";
        $html .= "<hr>";
        $html .= "<div id='tweet-controls'>";
        $html .=    "<div class='tweet-control tweet-score'>" . $this->data->score . "</div>";
        if ($auth->logged_in) {
            $html .=    "<div class='tweet-control'><a href='" . $router->urlFor("like", ['tweet' => $this->data->id, 'user' => $_SESSION['user_login']]) . "'><img src='" . $router->iconesPath() . "like.png' /></a></div>";
            $html .=    "<div class='tweet-control'><a href='" . $router->urlFor("dislike", ['tweet' => $this->data->id, 'user' => $_SESSION['user_login']]) . "'><img src='" . $router->iconesPath() . "dislike.png' /></a></div>";
            $html .=    "<div class='tweet-control'><a href='" . $router->urlFor("follow", ['user' => $_SESSION['user_login'], 'followee' => $this->data->author]) . "'><img src='" . $router->iconesPath() . "following.png' /></a></div>";
        }            
        $html .= "</div>";
        $html .= "\t\t\t</div>";

        return $html;
    }



    /* Méthode renderPostTweet
     *
     * Realise la vue de régider un Tweet
     *
     */
    protected function renderPostTweet(){
        
        /* Méthode renderPostTweet
         *
         * Retourne la framgment HTML qui dessine un formulaire pour la rédaction 
         * d'un tweet, l'action (bouton de validation) du formulaire est la route "/send/"
         *
         */ 
        $router = new Router();
        $html = "\t\t\t<form id='tweet-form' class='forms method='post' action='" . $router->urlFor('newTweet') . "'>\n";
        $html .= "\t\t\t\t<textarea id='text' class='forms-text name='text' placeholder='Enter tweet...'></textarea>\n";
        $html .= "\t\t\t\t<input type='hidden' id='user' name='user' value='" . $_SESSION['user_login'] . "'>";
        $html .= "\t\t\t\t<input type='submit' value='Send' class='forms-button'>\n";
        $html .= "\t\t\t</form>\n";

        return $html;
        
    }

    protected function renderLogin() {
        $router = new Router();
        $html = "<form method='post' action='" . $router->urlFor('connexion') . "'>\n";
        $html .= "<input type='text' name='username' id='username' placeholder='username'>\n";
        $html .= "<input type='password' name='pass' id='pass'>\n";
        $html .= "<input type='submit' value='login'>\n";
        $html .= "</form>";

        return $html;
    }

    protected function renderFollowers() {
        $followers = $this->data->followedBy()->get();
        $html = "<div>";
        foreach($followers as $follower) {
            $html .= "<p>" . $follower->fullname . "</p>";
        }
        $html .= "</div>";

        return $html;
    }

    protected function renderFollowed() {
        $followed = $this->data->follows()->get();
        $html = "<div>";
        $html .= "<h3>Followed by " . $this->data->followers . " miniTweetos</h2>";
        $html .= "<h3>Currently following</h2>";
        foreach($followed as $followee) {
            $html .= "<p>" . $followee->fullname . "</p>";
        }
        $html .= "</div>";

        return $html;
    }

    protected function renderSignup() {
        $router = new Router();
        $html = "<div>";
        $html .= "<form method='post' action='" . $router->urlFor('signup') . "'>";
        $html .= "<input type='text' name='fullname' placeholder='enter your full name'>";
        $html .= "<input type='text' name='username' placeholder='choose an username'>";
        $html .= "<input type='password' name='password' placeholder='***'>";
        $html .= "<input type='submit' value='Sign Up'>";

        return $html;
    }

    private function renderAddTwtButton() {
        $router = new Router();
        $html = "<div id='addTwtButton' class='theme-backcolor1'>";
        $html .= "<a href='" . $router->urlFor('formTweet') . "'><button class='forms-button'>New</button></a>";
        $html .= "</div>";
        return $html;
    }


    /* Méthode renderBody
     *
     * Retourne la framgment HTML de la balise <body> elle est appelée
     * par la méthode héritée render.
     *
     */
    
    protected function renderBody($selector){

        /*
         * voire la classe AbstractView
         * 
         */
        $auth = new Auth();
        $html = $this->renderHeader() . "\n";
        $html .= "\t\t<section class='theme-backcolor2'>\n";
        switch ($selector) {
            case 'home':
                $html .= $this->renderHome() . "\n";
                break;
            case 'tweet':
                $html .= $this->renderViewTweet() . "\n";
                break;
            case 'user':
                $html .= $this->renderUserTweets() . "\n";
                break;
            case 'tweetForm' :
                $html .= $this->renderPostTweet() . "\n";
                break;
            case 'login': 
                $html .= $this->renderLogin() . "\n";
                break;
            case 'followed':
                $html .= $this->renderFollowed() . "\n";
                break;
            case 'followers':
                $html .= $this->renderFollowers() . "\n";
                break;
            case 'signup':
                $html .= $this->renderSignup() . "\n";
                break;
            default:
                $html .= $this->renderHome() . "\n";
                break;
        }
        if ($auth->logged_in) {
            $html .= $this->renderAddTwtButton() . "\n";
        }
        $html .= "</section>";
        $html .= $this->renderFooter();

        return $html;
    }    
}
