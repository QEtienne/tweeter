<?php
    namespace tweeterapp\model;

    class User extends \Illuminate\DataBase\Eloquent\Model {
        protected $table = 'user'; /* nom de la table */
        protected $primaryKey = 'id'; /* nom de la clé primaire */
        public $timestamps = false; /*si vrai la table contient les colonnes update_at et created_at */
        
        public function tweets() {
            return $this->hasMany('tweeterapp\model\Tweet', 'author');
        }

        public function liked() {
            return $this->belongsToMany('tweeterapp\model\Tweet', 'like', 'user_id', 'tweet_id');
        }

        public function followedBy() {
            return $this->belongsToMany('tweeterapp\model\User', 'follow', 'followee', 'follower');
        }

        public function follows() {
            return $this->belongsToMany('tweeterapp\model\User', 'follow', 'follower', 'followee');
        }
    }
?>