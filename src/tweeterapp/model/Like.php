<?php
    namespace tweeterapp\model;

    class Like extends \Illuminate\DataBase\Eloquent\Model {
        protected $table = 'like';
        protected $primaryKey = 'id';
        public $timestamps = false;
    }
?>