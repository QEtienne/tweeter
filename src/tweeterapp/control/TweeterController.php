<?php

namespace tweeterapp\control;

use tweeterapp\model\Tweet;
use tweeterapp\model\User;
use mf\router\Router;
use \tweeterapp\view\TweeterView;
use \tweeterapp\model\Like;
use \tweeterapp\model\Follow;

/* Classe TweeterController :
 *  
 * Réalise les algorithmes des fonctionnalités suivantes: 
 *
 *  - afficher la liste des Tweets 
 *  - afficher un Tweet
 *  - afficher les tweet d'un utilisateur 
 *  - afficher la le formulaire pour poster un Tweet
 *  - afficher la liste des utilisateurs suivis 
 *  - évaluer un Tweet
 *  - suivre un utilisateur
 *   
 */

class TweeterController extends \mf\control\AbstractController {


    /* Constructeur :
     * 
     * Appelle le constructeur parent
     *
     * c.f. la classe \mf\control\AbstractController
     * 
     */
    
    public function __construct(){
        parent::__construct();
    }


    /* Méthode viewHome : 
     * 
     * Réalise la fonctionnalité : afficher la liste de Tweet
     * 
     */
    
    public function viewHome(){

        /* Algorithme :
         *  
         *  1 Récupérer tout les tweet en utilisant le modèle Tweet
         *  2 Parcourir le résultat 
         *      afficher le text du tweet, l'auteur et la date de création
         *  3 Retourner un block HTML qui met en forme la liste
         * 
         */

        $tweets = Tweet::select()
                        ->orderBy('created_at', 'desc')
                        ->get();
        $view = new TweeterView($tweets);
        return $view->render('home');
    }
    /**
     * Méthode viewPersonalHome :
     * Réalise la fonctionnalité : afficher la liste des tweets des personnes que je suis.
     */
    public function viewPersonalHome() {        
        $user = User::select()
                        ->where('username', '=', $_SESSION['user_login'])
                        ->with('follows')
                        ->first();
        $followees = $user->follows;
        $author = [];
        foreach ($followees as $followee) {
            $author[] = $followee->id;
        }
        $tweets = Tweet::select()
                        ->whereIn('author', $author)
                        ->orderBy('created_at', 'desc')
                        ->get();
        
        $view = new TweeterView($tweets);
        return $view->render('home');
    }

    /* Méthode viewTweet : 
     *  
     * Réalise la fonctionnalité afficher un Tweet
     *
     */
    
    public function viewTweet(){

        /* Algorithme : 
         *  
         *  1 L'identifiant du Tweet en question est passé en paramètre (id) 
         *      d'une requête GET 
         *  2 Récupérer le Tweet depuis le modèle Tweet
         *  3 Afficher toutes les informations du tweet 
         *      (text, auteur, date, score)
         *  4 Retourner un block HTML qui met en forme le Tweet
         * 
         *  Erreurs possibles : (*** à  implanter ultérieurement ***)
         *    - pas de paramètre dans la requête
         *    - le paramètre passé ne correspond pas a un identifiant existant
         *    - le paramètre passé n'est pas un entier 
         * 
         */
        if (array_key_exists('id', $this->request->get)) {
            if (intval($this->request->get['id']) > 0) {

                $tweet = Tweet::select()
                            ->where('id', '=', $this->request->get['id'])
                            ->first();
                if ($tweet != null) {
                    $view = new TweeterView($tweet);
                    return $view->render('tweet');
                } else {
                    throw new \Exception("This id doesn't exists");
                }
            } else {
                throw new \Exception("The id parameter must be an integer");
            }
        } else {
            throw new \Exception("This request need an id parameter");
        }
    }

    /* Méthode viewUserTweets :
     *
     * Réalise la fonctionnalité afficher les tweet d'un utilisateur
     *
     */
    
    public function viewUserTweets(){

        /*
         *
         *  1 L'identifiant de l'utilisateur en question est passé en 
         *      paramètre (id) d'une requête GET 
         *  2 Récupérer l'utilisateur et ses Tweets depuis le modèle 
         *      Tweet et User
         *  3 Afficher les informations de l'utilisateur 
         *      (non, login, nombre de suiveurs) 
         *  4 Afficher ses Tweets (text, auteur, date)
         *  5 Retourner un block HTML qui met en forme la liste
         *
         *  Erreurs possibles : (*** à  implanter ultérieurement ***)
         *    - pas de paramètre dans la requête
         *    - le paramètre passé ne correspond pas a un identifiant existant
         *    - le paramètre passé n'est pas un entier 
         * 
         */
        if (array_key_exists('id', $this->request->get)) {
            if (intval($this->request->get['id']) > 0) {
                $user = User::select()
                                ->where('id', '=', $this->request->get['id'])
                                ->first();
                if ($user != null) {
                    $view = new TweeterView($user);
                    return $view->render('user');
                } else {
                    throw new \Exception("This id doesn't exists");
                }
            } else {
                throw new \Exception("The id parameter must be an integer");
            } 
        } else {
            throw new \Exception("This request need an id parameter");
        }
    }

    /**
     * Méthode viewPostTweet :
     * Réalise la fonctionnalité afficher le formulaire 
     * pour rédiger et publier un tweet
    */
    public function viewPostTweet() {
        $view = new TweeterView(null);
        return $view->render('tweetForm');
    }

    public function viewFollowers() {
        $user = User::select()
                        ->where('username', '=', $_SESSION['user_login'])
                        ->first();
        $view = new TweeterView($user);
        $view->render('followers');
    }

    public function viewFollowed() {
        $user = User::select()
                        ->where('username', '=', $_SESSION['user_login'])
                        ->first();
        $view = new TweeterView($user);
        $view->render('followed');
    }

    /**
     * Méthode sendTweed :
     * Réalise la fonctionnalité envoyer un tweet
     */
    public function sendTweet() {
        $user = User::select()
                        ->where('username', '=', $_POST['user'])
                        ->first();
        $tweet = new Tweet();
        $tweet->text = $_POST['text'];
        $tweet->author = $user->id;
        $tweet->score = 0;
        $tweet->save();
        $router = new Router();
        header('Location: ' . $router->urlFor('maison'));
    }

    private function giveTweetNote($note, $user_name, $tweet_id) {
        $user = User::select()
                        ->where('username', '=', $user_name)
                        ->first();
        $tweet = Tweet::select()
                        ->where('id', '=', $tweet_id)
                        ->first();
        $like_old = Like::select()
                            ->where('tweet_id', '=', $tweet->id)
                            ->where('user_id', '=', $user->id)
                            ->first();
        if ($like_old == null) {
            $tweet->score += $note;
            $tweet->save();
            $like = new Like();
            $like->user_id = $user->id;
            $like->tweet_id = $tweet->id;
            $like->update();
        }

        $router = new Router();
        header('Location: ' . $router->urlFor('tweet', ['id' => $tweet->id]));
    }

    public function likeTweet() {
        $this->giveTweetNote(1, $_GET['user'], $_GET['tweet']);
    }

    public function dislikeTweet() {
        $this->giveTweetNote(-1, $_GET['user'], $_GET['tweet']);
    }

    public function followSomeone() {
        $user = User::select()
                        ->where('username', '=', $_GET['user'])
                        ->first();
        $follow_old = Follow::select()
                                ->where('follower', '=', $user->id)
                                ->where('followee', '=', $_GET['followee'])
                                ->first();
        if ($follow_old == null) {
            $followed = User::select()
                                ->where('id', '=', $_GET['followee'])
                                ->first();
            $followed->followers += 1;
            $followed->update();
            $follow = new Follow();
            $follow->follower = $user->id;
            $follow->followee = $_GET['followee'];
            $follow->save();
        }

        $router = new Router();
        header('Location: ' . $router->urlFor("following"));
    }
}
