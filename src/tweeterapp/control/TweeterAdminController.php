<?php
    namespace tweeterapp\control;

    /**
     * TODO: contrôler les données formulaires
     */

    use \tweeterapp\model\Tweet;
    use \tweeterapp\model\Follow;
    use \tweeterapp\view\TweeterView;
    use \tweeterapp\view\TweeterAdminView;
    use \tweeterapp\auth\TweeterAuthentification as Auth;
    use \tweeterapp\model\User;
    use \mf\router\Router;

    class TweeterAdminController extends \mf\control\AbstractController {

        public function viewLogin() {
            $view = new TweeterView(null);
            return $view->render('login');
        }

        public function viewSignup() {
            $view = new TweeterView(null);
            return $view->render('signup');
        }

        public function checkLogin() {
            $auth = new Auth();
            
            try {
                $auth->loginUser($_POST['username'], $_POST['pass']);
                $user = User::select()
                                ->where('username', '=', $_SESSION['user_login'])
                                ->first();
                $view = new TweeterView($user);
                $view->render('followers');
            } catch (\Exception $e) {
                Router::executeRoute('loginForm');
            }
        }

        public function checkSignUp() {
            $auth = new Auth;
            try {
                $auth->createUser($_POST['username'], $_POST['password'], $_POST['fullname']);
                Router::executeRoute('maison');
            } catch (\Exception $e) {
                Router::executeRoute('signupform');
            }
        }

        public function logout() {
            $auth = new Auth();
            $auth->logout();
            Router::executeRoute('maison');
        }

        public function viewAdmin() {
            $view = new TweeterAdminView(null);
            return $view->render(null);
        }

        public function viewUsersList() {
            $users = User::select()
                            ->where('level', '<', 200)
                            ->orderBy('followers', 'desc')
                            ->get();
            $view = new TweeterAdminView($users);
            return $view->render("list");
        }

        public function viewUserFollowers() {
            $follows = Follow::select()
                            ->where('followee', '=', $_GET['id'])
                            ->get();
            
            $followersId = [];

            foreach ($follows as $f) {
                $followersId[] = $f->follower;
            }
            $followers = User::select()
                                ->whereIn('id', $followersId)
                                ->get();
            $view = new TweeterAdminView($followers);
            return $view->render("followers");
        }

        public function viewSphere() {
            $users = User::select()
                            // ->where('level', '<', 200)
                            ->get();
            $usersWithSphere = [];
            foreach ($users as $user) {
                $usersWithSphere[] = [$user, $this->sphere($user)];
            }
            usort($usersWithSphere, function ($a, $b) {
                if ($a[1] == $b[1]) {
                    return 0;
                }
                return ($a[1] < $b[1]) ? 1 : -1;
            });
            $view = new TweeterAdminView($usersWithSphere);
            return $view->render("sphere");
        }

        private function sphere($user, $alreadyKnowed = []) {
            $sphere = 0;

            if (array_search($user->id, $alreadyKnowed) === false) {
                $alreadyKnowed[] = $user->id;
                $follows = Follow::select()
                                    ->where('followee', '=', $user->id)
                                    ->get();
                $followersId = [];
                foreach ($follows as $f) {
                    $followersId[] = $f->follower;
                }
                $followers = User::select()
                                    ->whereIn('id', $followersId)
                                    ->get();
                
                foreach ($followers as $follower) {
                    if (array_search($follower->id, $alreadyKnowed) === false) {
                        $sphere += 1 + $this->sphere($follower, $alreadyKnowed);
                        $alreadyKnowed[] = $follower->id;
                    }
                }
            }
            return $sphere;;
        }
    }
?>